package de.szut.store.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.szut.store.model.Supplier;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class AddArticleDto {
    @NotBlank(message = "Designation is mandatory")
    private String designation;

    @NotNull
    private Double price;
}
