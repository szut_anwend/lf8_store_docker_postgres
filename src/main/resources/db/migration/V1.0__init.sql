create table contact (
    cid serial primary key,
    street varchar(50),
    city varchar(50),
    postcode char(5),
    phone varchar(15)
);

create table supplier(
    sid serial primary key,
    name varchar(65),
    contact_id integer not null,
    constraint fk_contact foreign key(contact_id) references contact(cid)
);

create table article(
   aid serial primary key,
   designation varchar(50) not null,
   price Numeric(10,2),
   createDate timestamp default CURRENT_TIMESTAMP not null,
   lastUpdateDate timestamp default CURRENT_TIMESTAMP not null,
   supplier_id integer not null,
   constraint fk_supplier foreign key(supplier_id) references supplier(sid)
);